package br.com.teste.projeto.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.teste.projeto.domain.Cliente;
import br.com.teste.projeto.repository.ClienteRepository;

@Service
public class ClienteService {
	
	private final ClienteRepository clienteRepository;
	
	private final Logger log = LoggerFactory.getLogger(ClienteService.class);
	
	
	public ClienteService(ClienteRepository clienteRepository) {
		this.clienteRepository = clienteRepository;
	}
	
	public Cliente save(Cliente cliente) { 
		log.debug("Request to save Cliente : {}", cliente);
		return clienteRepository.save(cliente);
	}
	
	public List<Cliente> findAllClientes() {
		log.debug("Request to findAll Clientes");
		return clienteRepository.findAll();
	}
	
	public void deleteCliente (Long id) {
		log.debug("Request to delete Cliente : {}", id);
		clienteRepository.delete(clienteRepository.findById(id).get());
	}
	
   public Cliente update(Cliente cliente) {
       log.debug("Request to update Cliente : {}", cliente);
       return save(cliente);
   }
   
   public Optional<Cliente> findOne(Long cliente) {
       log.debug("Request to findOne Cliente : {}", cliente);
       return clienteRepository.findById(cliente);
   }

}
