package br.com.teste.projeto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.teste.projeto.domain.Fatura;

@Repository
public interface FaturaRepository extends JpaRepository<Fatura, Long>{

}
