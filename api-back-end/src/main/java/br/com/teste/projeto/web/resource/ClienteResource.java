package br.com.teste.projeto.web.resource;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.teste.projeto.domain.Cliente;
import br.com.teste.projeto.service.ClienteService;

/**
 * REST controller for managing {@link br.com.teste.projeto.domain.Cliente}.
 */
@RestController
@RequestMapping("/api")
public class ClienteResource {
	
	public static final String ENTITY_NAME = "cliente";
    private final Logger log = LoggerFactory.getLogger(ClienteResource.class);
    private final ClienteService clienteService;


    public ClienteResource(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @PostMapping("/cliente")
    public ResponseEntity<Cliente> createCliente(@RequestBody Cliente cliente) throws Exception {
        log.debug("REST request to save Cliente : {}", cliente);
        if (cliente.getId() != null) {
            throw new Exception("A new cliente cannot already have an ID");
        }
        Cliente result = clienteService.save(cliente);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping("/cliente")
    public ResponseEntity<Cliente> updateCliente(@RequestBody Cliente cliente) throws Exception {
        log.debug("REST request to update Cliente : {}", cliente);
        if (cliente.getId() == null) {
            throw new Exception("Invalid id");
        }
        Cliente result = clienteService.update(cliente);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/cliente")
    public ResponseEntity<List<Cliente>> getAllClientes() {
        log.debug("REST request to get a list of Clientes");
        return new ResponseEntity<List<Cliente>>(clienteService.findAllClientes(), HttpStatus.OK);
    }

    @GetMapping("/cliente/{id}")
    public ResponseEntity<Cliente> getCliente(@PathVariable Long id) {
        log.debug("REST request to get Cliente : {}", id);
        Optional<Cliente> cliente = clienteService.findOne(id);
        return new ResponseEntity<>(cliente.get(), HttpStatus.OK);
    }


    @DeleteMapping("/cliente/{id}")
    public ResponseEntity<Void> deleteCliente(@PathVariable Long id) {
        log.debug("REST request to delete Cliente : {}", id);
        clienteService.deleteCliente(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
