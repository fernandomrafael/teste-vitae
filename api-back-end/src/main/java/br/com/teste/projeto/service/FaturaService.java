package br.com.teste.projeto.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.teste.projeto.domain.Fatura;
import br.com.teste.projeto.repository.FaturaRepository;

@Service
public class FaturaService {
	
	private final FaturaRepository faturaRepository;
	
	private final Logger log = LoggerFactory.getLogger(FaturaService.class);
	
	
	public FaturaService(FaturaRepository faturaRepository) {
		this.faturaRepository = faturaRepository;
	}
	
	public Fatura save(Fatura fatura) { 
		log.debug("Request to save fatura : {}", fatura);
		return faturaRepository.save(fatura);
	}
	
	public List<Fatura> findAllFaturas() {
		log.debug("Request to findAll fatura");
		return faturaRepository.findAll();
	}
	
	public void deleteFatura (Long id) {
		log.debug("Request to delete fatura : {}", id);
		faturaRepository.delete(faturaRepository.findById(id).get());
	}
	
   public Fatura update(Fatura fatura) {
       log.debug("Request to update fatura : {}", fatura);
       return save(fatura);
   }
   
   public Optional<Fatura> findOne(Long id) {
       log.debug("Request to findOne fatura : {}", id);
       return faturaRepository.findById(id);
   }

}
