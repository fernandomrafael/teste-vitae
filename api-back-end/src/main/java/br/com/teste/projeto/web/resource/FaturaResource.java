package br.com.teste.projeto.web.resource;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.teste.projeto.domain.Fatura;
import br.com.teste.projeto.service.FaturaService;

@Service
public class FaturaResource {
	
	public static final String ENTITY_NAME = "fatura";
    private final Logger log = LoggerFactory.getLogger(ClienteResource.class);
    private final FaturaService faturaService;


    public FaturaResource(FaturaService faturaService) {
        this.faturaService = faturaService;
    }

    @PostMapping("/fatura")
    public ResponseEntity<Fatura> createFatura(@RequestBody Fatura fatura) throws Exception {
        log.debug("REST request to save fatura : {}", fatura);
        if (fatura.getId() != null) {
            throw new Exception("A new fatura cannot already have an ID");
        }
        Fatura result = faturaService.save(fatura);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping("/fatura")
    public ResponseEntity<Fatura> updateCliente(@RequestBody Fatura fatura) throws Exception {
        log.debug("REST request to update fatura : {}", fatura);
        if (fatura.getId() == null) {
            throw new Exception("Invalid id");
        }
        Fatura result = faturaService.update(fatura);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/fatura")
    public ResponseEntity<List<Fatura>> getAllClientes() {
        log.debug("REST request to get a list of faturas");
        return new ResponseEntity<List<Fatura>>(faturaService.findAllFaturas(), HttpStatus.OK);
    }

    @GetMapping("/fatura/{id}")
    public ResponseEntity<Fatura> getFatura(@PathVariable Long id) {
        log.debug("REST request to get fatura : {}", id);
        Optional<Fatura> fatura = faturaService.findOne(id);
        return new ResponseEntity<>(fatura.get(), HttpStatus.OK);
    }


    @DeleteMapping("/fatura/{id}")
    public ResponseEntity<Void> deleteFatura(@PathVariable Long id) {
        log.debug("REST request to delete fatura : {}", id);
        faturaService.deleteFatura(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
